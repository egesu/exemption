<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\ExemptionType;

class ExemptionTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $list = ExemptionType::orderBy('title', 'asc')
            ->get();

        return view('exemption-type.index', [
            'list' => $list,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('exemption-type.form', [
            'exemptionType' => new ExemptionType(),
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $formData = $request->validate([
            'title' => 'required|string|max:20',
        ]);

        ExemptionType::create($formData);

        return redirect()->to(route('exemption-type.index'))
            ->with('success', __('Yeni muafiyet türü yaratıldı'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\ExemptionType  $exemptionType
     * @return \Illuminate\Http\Response
     */
    public function show(ExemptionType $exemptionType)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\ExemptionType  $exemptionType
     * @return \Illuminate\Http\Response
     */
    public function edit(ExemptionType $exemptionType)
    {
        return view('exemption-type.form', [
            'exemptionType' => $exemptionType,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\ExemptionType  $exemptionType
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ExemptionType $exemptionType)
    {
        $formData = $request->validate([
            'title' => 'required|string|max:20',
        ]);

        $exemptionType->fill($formData);
        $exemptionType->save();

        return redirect()->to(route('exemption-type.index'))
            ->with('success', __('Muafiyet türü kaydedildi'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ExemptionType  $exemptionType
     * @return \Illuminate\Http\Response
     */
    public function destroy(ExemptionType $exemptionType)
    {
        $exemptionType->delete();

        return redirect()->back()
            ->with('success', __('Muafiyet türü silindi'));
    }
}
