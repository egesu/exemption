<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Fee;

class FeeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $feeList = Fee::orderBy('starts_at', 'desc')
            ->get();

        return view('fee.index', [
            'feeList' => $feeList,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('fee.form', [
            'fee' => new Fee(),
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $formData = $request->validate([
            'starts_at' => 'required|date_format:Y-m-d',
            'ends_at' => 'date_format:Y-m-d|nullable',
            'amount' => 'numeric|min:0.01|max:999.99',
        ]);

        Fee::create($formData);

        return redirect()->to(route('fee.index'))
            ->with('success', __('Yeni ödenti fiyatı yaratıldı'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Fee  $fee
     * @return \Illuminate\Http\Response
     */
    public function show(Fee $fee)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Fee  $fee
     * @return \Illuminate\Http\Response
     */
    public function edit(Fee $fee)
    {
        return view('fee.form', [
            'fee' => $fee,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Fee  $fee
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Fee $fee)
    {
        $formData = $request->validate([
            'starts_at' => 'required|date',
            'ends_at' => 'date|nullable',
            'amount' => 'numeric|min:0.01|max:999.99',
        ]);

        $fee->fill($formData);
        $fee->save();

        return redirect()->to(route('fee.index'))
            ->with('success', __('Ödenti fiyatı kaydedildi'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Fee  $fee
     * @return \Illuminate\Http\Response
     */
    public function destroy(Fee $fee)
    {
        $fee->delete();

        return redirect()->back()
            ->with('success', __('Ödenti silindi'));
    }
}
