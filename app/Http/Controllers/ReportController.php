<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use App\Models\Exemption;
use App\Models\ExemptionType;
use App\Models\ExemptionUser;
use App\Models\User;

class ReportController extends Controller
{
    public function index(Request $request)
    {
        $list = $this->getList($request);

        return view('report.index', [
            'list' => $list,
            'exemptionTypeList' => ExemptionType::orderBy('title')->get(),
        ]);
    }

    public function getCSV(Request $request)
    {
        $list = $this->getList($request);
        $filePath = storage_path('report.csv');

        $out = fopen($filePath, 'w');

        fputcsv($out, [
            __('Sicil no.'),
            __('Ad'),
            __('Soyadı'),
            __('Güncel muafiyet'),
            __('Başlama tarihi'),
            __('Toplam ay sayısı'),
            __('Toplam düşülen ay sayısı'),
            __('Toplam muaf ay sayısı'),
            __('Toplam eksiltilecek tutar'),
        ]);

        foreach ($list as $item) {
            fputcsv($out, [
                $item->code,
                $item->name,
                $item->last_name,
                !empty($item->currentExemption) ? $item->currentExemption->exemptionType->title : '',
                !empty($item->currentExemption) ? $item->currentExemption->starts_at->format('d.m.Y') : '',
                $item->totalMonthCount,
                $item->totalSubstractedMonthCount,
                $item->totalExemptedMonthCount,
                $item->totalFeeToSubstract,
            ]);
        }

        fclose($out);

        // $response = file_get_contents($filePath);
        // unlink($filePath);

        return response()
            ->download($filePath, 'rapor.csv', [
                'Content-Type' => 'text/csv',
            ])
            ->deleteFileAfterSend();
        // ->header('Content-Type', 'text/csv');
    }

    protected function getList(Request $request)
    {
        $filterData = $request->validate([
            'code_start' => 'int|nullable',
            'code_end' => 'int|nullable',
            'exemption_type_id' => 'int|nullable',
            'name' => 'string|nullable',
            'last_name' => 'string|nullable',
        ]);

        $query = User::leftJoin('exemptions AS e', 'e.user_id', '=', 'users.id')
            ->leftJoin('exemptions AS e_all', 'e.user_id', '=', 'users.id');

        if (!empty($filterData['code_start'])) {
            $query->where('users.code', '>=', $filterData['code_start']);
        }

        if (!empty($filterData['code_end'])) {
            $query->where('users.code', '<=', $filterData['code_end']);
        }

        if (!empty($filterData['exemption_type_id'])) {
            $query->whereNull('e.ends_at')
                ->where(
                    'e.exemption_type_id',
                    $filterData['exemption_type_id']
                );
        }

        if (!empty($filterData['name'])) {
            $query->where('users.name', $filterData['name']);
        }

        if (!empty($filterData['last_name'])) {
            $query->where('users.last_name', $filterData['last_name']);
        }

        $query->groupBy('users.id')
            ->select([
                'users.id',
                'users.name',
                'users.last_name',
                'users.code',
                DB::raw('SUM(e_all.substracted_month_count) AS substracted_month_count'),
            ])
            ->with([
                'currentExemption',
                'currentExemption.exemptionType',
                'exemptions',
            ]);

        $list = $query->get();

        foreach ($list as $user) {
            $totalMonthCount = 0;
            $totalSubstractedMonthCount = 0;
            $totalExemptedMonthCount = 0;
            $totalFeeToSubstract = 0;

            foreach ($user->exemptions as $exemption) {
                if (is_null($exemption->ends_at)) {
                    continue;
                }
                $totalMonthCount += $exemption->month_count;
                $totalSubstractedMonthCount += $exemption->substracted_month_count;
                $totalExemptedMonthCount += $exemption->exempted_month_count;
                $totalFeeToSubstract += $exemption->fee_to_substract;
            }

            $user->totalMonthCount = $totalMonthCount;
            $user->totalSubstractedMonthCount = $totalSubstractedMonthCount;
            $user->totalExemptedMonthCount = $totalExemptedMonthCount;
            $user->totalFeeToSubstract = $totalFeeToSubstract;
        }

        return $list;
    }
}
