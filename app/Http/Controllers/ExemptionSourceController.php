<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\ExemptionSource;

class ExemptionSourceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $list = ExemptionSource::orderBy('title', 'asc')
            ->get();

        return view('exemption-source.index', [
            'list' => $list,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('exemption-source.form', [
            'exemptionSource' => new ExemptionSource(),
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $formData = $request->validate([
            'title' => 'required|string|max:20',
        ]);

        ExemptionSource::create($formData);

        return redirect()->to(route('exemption-source.index'))
            ->with('success', __('Yeni bilgi kaynağı yaratıldı'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\ExemptionSource  $exemptionSource
     * @return \Illuminate\Http\Response
     */
    public function show(ExemptionSource $exemptionSource)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\ExemptionSource  $exemptionSource
     * @return \Illuminate\Http\Response
     */
    public function edit(ExemptionSource $exemptionSource)
    {
        return view('exemption-source.form', [
            'exemptionSource' => $exemptionSource,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\ExemptionSource  $exemptionSource
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ExemptionSource $exemptionSource)
    {
        $formData = $request->validate([
            'title' => 'required|string|max:20',
        ]);

        $exemptionSource->fill($formData);
        $exemptionSource->save();

        return redirect()->to(route('exemption-source.index'))
            ->with('success', __('Bilgi kaynağı kaydedildi'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ExemptionSource  $exemptionSource
     * @return \Illuminate\Http\Response
     */
    public function destroy(ExemptionSource $exemptionSource)
    {
        $exemptionSource->delete();

        return redirect()->back()
            ->with('success', __('Bilgi kaynağı silindi'));
    }
}
