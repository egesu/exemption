<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Exemption;
use App\Models\ExemptionSource;
use App\Models\ExemptionType;
use App\Models\User;

class ExemptionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $filters = $request->validate([
            'code' => 'int|nullable|sometimes',
            'name' => 'string|nullable|sometimes',
            'last_name' => 'string|nullable|sometimes',
        ]);

        $user = null;
        $currentExemption = null;
        $totalSubstractedMonthCount = 0;
        $totalMonthCount = 0;
        $totalExemptedMonthCount = 0;
        $totalFeeToSubstract = 0;

        if (empty($filters)) {
            $list = collect();
        } else {
            $user = new User();

            $query = Exemption::join('users AS u', 'u.id', '=', 'exemptions.user_id')
                ->select('exemptions.*');

            if (array_key_exists('code', $filters)) {
                $user = User::where('code', $filters['code'])
                    ->firstOrFail();

                $query->where('u.code', $user->code);
            }


            $list = $query
                ->with([
                    'user',
                    'exemptionType',
                    'exemptionSource',
                ])
                ->orderBy('exemptions.starts_at', 'desc')
                ->get();

            foreach ($list as $item) {
                if (!empty($item->ends_at)) {
                    $totalSubstractedMonthCount += $item->substracted_month_count;
                    $totalMonthCount += $item->month_count;
                    $totalExemptedMonthCount += $item->exempted_month_count;
                    $totalFeeToSubstract += $item->fee_to_substract;
                }
            }

            if ($list->count() and is_null($list->first()->ends_at)) {
                $currentExemption = $list->first();
            }
        }

        return view('exemption.index', [
            'list' => $list,
            'user' => $user,
            'currentExemption' => $currentExemption,
            'totalSubstractedMonthCount' => $totalSubstractedMonthCount,
            'totalExemptedMonthCount' => $totalExemptedMonthCount,
            'totalFeeToSubstract' => $totalFeeToSubstract,
            'totalMonthCount' => $totalMonthCount,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $form = $request->validate([
            'user_id' => 'required|integer',
        ]);

        $user = User::findOrFail($form['user_id']);
        $exemption = new Exemption();

        $typeList = ExemptionType::all();
        $sourceList = ExemptionSource::all();

        return view('exemption.form', [
            'user' => $user,
            'exemption' => $exemption,
            'typeList' => $typeList,
            'sourceList' => $sourceList,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $formData = $request->validate([
            'starts_at' => 'required|date_format:Y-m-d',
            'ends_at' => 'date_format:Y-m-d|nullable',
            'user_id' => 'required|exists:users,id',
            'exemption_type_id' => 'required|exists:exemption_types,id',
            'exemption_source_id' => 'required|exists:exemption_sources,id',
            'exemption_source_at' => 'required|date_format:Y-m-d',
            'exemption_info' => 'required|string',
            'substracted_month_count' => 'required|int|min:0|max:999',
        ]);

        $exemption = Exemption::create($formData);

        return redirect()->to(route('exemption.index', [
            'code' => $exemption->user->code,
        ]))->with('success', __('Yeni muafiyet yaratıldı'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Exemption  $exemption
     * @return \Illuminate\Http\Response
     */
    public function show(Exemption $exemption)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Exemption  $exemption
     * @return \Illuminate\Http\Response
     */
    public function edit(Exemption $exemption)
    {
        $user = $exemption->user;

        $typeList = ExemptionType::all();
        $sourceList = ExemptionSource::all();

        return view('exemption.form', [
            'user' => $user,
            'exemption' => $exemption,
            'typeList' => $typeList,
            'sourceList' => $sourceList,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Exemption  $exemption
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Exemption $exemption)
    {
        $formData = $request->validate([
            'starts_at' => 'required|date_format:Y-m-d',
            'ends_at' => 'date_format:Y-m-d|nullable',
            'exemption_type_id' => 'required|exists:exemption_types,id',
            'exemption_source_id' => 'required|exists:exemption_sources,id',
            'exemption_source_at' => 'required|date_format:Y-m-d',
            'exemption_info' => 'required|string',
            'substracted_month_count' => 'required|int|min:0|max:999',
        ]);

        $exemption->fill($formData);
        $exemption->save();

        return redirect()->to(route('exemption.index', [
            'code' => $exemption->user->code,
        ]))->with('success', __('Muafiyet güncellendi'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Exemption  $exemption
     * @return \Illuminate\Http\Response
     */
    public function destroy(Exemption $exemption)
    {
        $exemption->delete();

        return redirect()->back()
            ->with('success', __('Muafiyet silindi'));
    }
}
