<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

use App\Models\User;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $list = User::orderBy('name', 'asc')
            ->get();

        return view('user.index', [
            'list' => $list,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('user.form', [
            'user' => new User(),
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $formData = $request->validate([
            'name' => 'required|string|max:40',
            'last_name' => 'required|string|max:40',
            'code' => 'required|integer|min:0|max:999999|unique:users,code',
        ]);

        User::create($formData);

        return redirect()->to(route('user.index'))
            ->with('success', __('Yeni üye yaratıldı'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        return view('user.form', [
            'user' => $user,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        $formData = $request->validate([
            'name' => 'required|string|max:40',
            'last_name' => 'required|string|max:40',
            'code' => [
                'required',
                'integer',
                'min:0',
                'max:999999',
                Rule::unique('users')->ignore($user->id),
            ],
        ]);

        $user->fill($formData);
        $user->save();

        return redirect()->to(route('user.index'))
            ->with('success', __('Üye kaydedildi'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        $user->delete();

        return redirect()->back()
            ->with('success', __('Kullanıcı silindi'));
    }
}
