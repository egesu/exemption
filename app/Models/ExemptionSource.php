<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ExemptionSource extends Model
{
    protected $fillable = [
        'title',
    ];
}
