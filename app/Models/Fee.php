<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Fee extends Model
{
    protected $fillable = [
        'starts_at',
        'ends_at',
        'amount',
    ];

    public $casts = [
        'starts_at' => 'date',
        'ends_at' => 'date',
        'amount' => 'float',
    ];
}
