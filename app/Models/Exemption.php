<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Exemption extends Model
{
    protected $fillable = [
        'exemption_type_id',
        'exemption_source_id',
        'user_id',
        'starts_at',
        'ends_at',
        'exemption_source_at',
        'exemption_info',
        'substracted_month_count',
    ];

    public $casts = [
        'starts_at' => 'date',
        'ends_at' => 'date',
        'exemption_source_at' => 'date',
        'substracted_month_count' => 'int',
    ];

    public $appends = [
        'month_count',
        'exempted_month_count',
        'fee_to_substract',
    ];

    public function exemptionType()
    {
        return $this->belongsTo(ExemptionType::class, 'exemption_type_id');
    }

    public function exemptionSource()
    {
        return $this->belongsTo(ExemptionSource::class, 'exemption_source_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function getMonthCountAttribute()
    {
        $endsAt = $this->ends_at ?: now();
        $dayDiff = $endsAt->diffInDays($this->starts_at);
        $daysLeft = $dayDiff % 30;

        if ($daysLeft > 10) {
            return ceil($dayDiff / 30);
        }

        return floor($dayDiff / 30);
    }

    public function getExemptedMonthCountAttribute()
    {
        return $this->month_count - $this->substracted_month_count;
    }

    public function getFeeToSubstractAttribute(): float
    {
        $model = $this;

        if (empty($model->ends_at)) {
            $fee = Fee::whereNull('ends_at')->first();
        } else {
            $fee = Fee::where('starts_at', '<=', $this->starts_at)
                ->where(function ($query) use ($model) {
                    $query->whereDate('ends_at', '>=', $model->ends_at)
                        ->orWhereNull('ends_at');
                })
                ->first();
        }

        if (!empty($fee)) {
            return $this->exempted_month_count * $fee->amount;
        }

        $feeStart = Fee::where('starts_at', '<=', $this->starts_at)
            ->orderBy('starts_at', 'desc')
            ->first();

        if (empty($feeStart)) {
            // There is no fee for this date
            $feeStart = Fee::where('starts_at', '>=', $this->starts_at)
                ->orderBy('starts_at', 'asc')
                ->first();
        }

        $daysFirst = $this->starts_at->diffInDays($feeStart->ends_at);

        if ($daysFirst % 30 > 10) {
            $amountFirst = ceil($daysFirst / 30) * $feeStart->amount;
        } else {
            $amountFirst = floor($daysFirst / 30) * $feeStart->amount;
        }

        $feeEnd = Fee::where('starts_at', '>=', $this->starts_at)
            ->orderBy('starts_at', 'asc')
            ->first();

        if (empty($feeEnd)) {
            // There is no fee for this date
            $feeEnd = Fee::where('starts_at', '<=', $this->starts_at)
            ->orderBy('starts_at', 'desc')
            ->first();
        }

        $daysLast = $this->ends_at->diffInDays($feeEnd->starts_at);

        if ($daysLast % 30 > 10) {
            $amountLast = ceil($daysLast / 30) * $feeEnd->amount;
        } else {
            $amountLast = floor($daysLast / 30) * $feeEnd->amount;
        }

        $amount = $amountFirst + $amountLast;

        if (!empty($this->substracted_month_count)) {
            $amount -= $this->substracted_month_count * $feeStart->amount;
        }

        return $amount;
    }
}
