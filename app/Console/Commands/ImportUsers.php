<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use App\Models\User;

class ImportUsers extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ba:import-users {--file=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $filePath = $this->option('file');

        if(empty($filePath)) {
            $this->error('file option is required.');
            return false;
        }

        if(!\is_file($filePath) or substr($filePath, -4) !== '.csv') {
            $this->error('Invalid file option.');
            return false;
        }

        $file = \fopen($filePath, 'r');

        if(!$file) {
            $this->error('File cannot be read.');
            return false;
        }

        $i = 0;

        while(false !== ($row = fgetcsv($file))) {
            $this->info($row[0] . ' - ' . $row[1] . ' '. $row[2]);
            $i++;

            User::firstOrCreate([
                'name' => $row[1],
                'last_name' => $row[2],
                'code' => $row[0],
            ]);
        }

        $this->info("$i items checked");
    }
}
