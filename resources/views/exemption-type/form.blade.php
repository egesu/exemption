@extends('layouts.app')

@section('content')
<div class="container">
  <h3 class="mb-5">{{ __('Muafiyet türü') }}</h3>

  @include('partials.notifications')

  <form
    method="post"
    @if($exemptionType->id)
      action="{{ route('exemption-type.update', $exemptionType->id) }}"
    @else
      action="{{ route('exemption-type.store') }}"
    @endif
  >
    @csrf

    @if($exemptionType->id)
      <input type="hidden" name="_method" value="put">
    @endif

    <div class="form-group">
      <label for="title">{{ __('Muafiyet türü') }}</label>
      <input
        type="text"
        name="title"
        id="title"
        class="form-control"
        required
        value="{{ old('title') ?: $exemptionType->title }}"
        maxlength="20"
      >
    </div>

    <div>
      <button type="submit" class="btn btn-primary btn-block">{{ __('Kaydet') }}</button>
    </div>
  </form>
</div>
@endsection
