@extends('layouts.app')

@section('content')
<div class="container">
  <h3 class="mb-5">{{ __('Üye') }}</h3>

  @include('partials.notifications')

  <form
    method="post"
    @if($user->id)
      action="{{ route('user.update', $user->id) }}"
    @else
      action="{{ route('user.store') }}"
    @endif
  >
    @csrf

    @if($user->id)
      <input type="hidden" name="_method" value="put">
    @endif

    <div class="form-group">
      <label for="code">{{ __('Sicil No.') }}</label>
      <input
        type="number"
        name="code"
        id="code"
        class="form-control form-control-sm"
        required
        value="{{ old('code') ?: $user->code }}"
        max="999999"
        min="1"
        step="1"
      >
    </div>

    <div class="form-group">
      <label for="name">{{ __('Ad') }}</label>
      <input
        type="text"
        name="name"
        id="name"
        class="form-control form-control-sm"
        required
        value="{{ old('name') ?: $user->name }}"
        maxlength="40"
      >
    </div>

    <div class="form-group">
      <label for="last_name">{{ __('Soyadı') }}</label>
      <input
        type="text"
        name="last_name"
        id="last_name"
        class="form-control form-control-sm"
        required
        value="{{ old('last_name') ?: $user->last_name }}"
        maxlength="40"
      >
    </div>

    <div>
      <button type="submit" class="btn btn-primary btn-block">{{ __('Kaydet') }}</button>
    </div>
  </form>
</div>
@endsection
