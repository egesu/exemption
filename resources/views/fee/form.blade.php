@extends('layouts.app')

@section('content')
<div class="container">
  <h3 class="mb-5">{{ __('Ödenti') }}</h3>

  @include('partials.notifications')

  <form
    method="post"
    @if($fee->id)
      action="{{ route('fee.update', $fee->id) }}"
    @else
      action="{{ route('fee.store') }}"
    @endif
  >
    @csrf

    @if($fee->id)
      <input type="hidden" name="_method" value="put">
    @endif

    <div class="form-group">
      <label for="starts_at">{{ __('Başlangıç tarihi') }}</label>
      <input
        type="date"
        name="starts_at"
        id="starts_at"
        class="form-control"
        required
        value="{{ old('starts_at') ?: (!empty($fee->starts_at) ? $fee->starts_at->format('Y-m-d') : '') }}"
      >
    </div>

    <div class="form-group">
      <label for="ends_at">{{ __('Bitiş tarihi') }}</label>
      <input
        type="date"
        name="ends_at"
        id="ends_at"
        class="form-control"
        value="{{ old('$fee->ends_at') ?: (!empty($fee->ends_at) ? $fee->ends_at->format('Y-m-d') : '') }}"
      >
    </div>

    <div class="form-group">
      <label for="amount">{{ __('Ödenti') }}</label>
      <input
        type="number"
        name="amount"
        id="amount"
        class="form-control"
        required
        min="0.01"
        step="0.01"
        max="999.99"
        value="{{ old('amount') ?: $fee->amount }}"
      >
    </div>

    <div>
      <button type="submit" class="btn btn-primary btn-block">{{ __('Kaydet') }}</button>
    </div>
  </form>
</div>
@endsection
