@extends('layouts.app')

@section('content')
<div class="container">
  <div class="row mb-5">
    <div class="col-12 col-sm-8 text-center text-sm-left">
      <h3>{{ __('Ödentiler') }}</h3>
    </div>
    <div class="col-12 col-sm-4 text-center text-sm-right">
      <a class="btn btn-outline-info" href="{{ route('fee.create') }}">
        {{ __('Yeni Ödenti Fiyatı') }}
      </a>
    </div>
  </div>

  @include('partials.notifications')

  <table class="table table-responsive-xs table-striped">
    <thead>
      <tr>
        <th>{{ __('Başlangıç tarihi') }}</th>
        <th>{{ __('Bitiş tarihi') }}</th>
        <th>{{ __('Ödenti') }}</th>
        <th></th>
      </tr>
    </thead>
    <tbody>
      @foreach($feeList as $fee)
      <tr>
        <td>{{ $fee->starts_at->format('d.m.Y') }}</td>
        <td>
          @if(!empty($fee->ends_at))
            {{ $fee->ends_at->format('d.m.Y') }}
          @else
            <span class="text-muted">{{ __('girilmemiş') }}</span>
          @endif
        </td>
        <td>{{ $fee->amount }} TL</td>
        <td class="text-right">
          <a class="btn btn-outline-info" href="{{ route('fee.edit', $fee->id) }}">{{ __('Düzenle') }}</a>
          <form class="d-inline" action="{{ route('fee.destroy', $fee->id) }}" method="post">
            @csrf
            <input type="hidden" name="_method" value="delete">
            <button
              type="submit"
              class="btn btn-outline-danger"
            >{{ __('Sil') }}</button>
          </form>
        </td>
      </tr>
      @endforeach
    </tbody>
  </table>
</div>
@endsection
