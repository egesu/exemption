@extends('layouts.app')

@section('content')
<div class="container-fluid">
  <div class="row mb-5">
    <div class="col-12 col-sm-8 text-center text-sm-left">
      <h3>
        {{ __('Muafiyet Kayıtları') }}
        @if(!empty($user) and $user->id)
          - <strong class="text-danger">
            {{ $user->name }} {{ $user->last_name }}
          </strong>
        @endif
      </h3>

      @if(!empty($currentExemption))
        {{ __('Güncel muafiyet:') }}
        <span class="text-danger">
          {{ $currentExemption->exemptionType->title }}
        </span>
      @endif
    </div>
    <div class="col-12 col-sm-4 text-center text-sm-right">
      @if(!empty($user) and $user->id)
        <a
          class="btn btn-outline-info"
          href="{{ route('exemption.create', ['user_id' => $user->id]) }}"
        >{{ __('Yeni Muafiyet Ekle') }}</a>
      @endif
    </div>
  </div>

  <form action="{{ route('exemption.index') }}" method="get" class="mb-3">
    <div class="form-group row">
      <label class="col-sm-2 col-lg-1 col-form-label" for="code">
        {{ __('Sicil No.') }}
      </label>
      <div class="col-sm-10 col-md-6 col-lg-4">
        <input
          class="form-control input-sm"
          type="number"
          min="1"
          max="999999"
          step="1"
          name="code"
          id="code"
          value="{{ request('code') }}"
        >
      </div>
    </div>

    <button type="submit" class="btn btn-outline-secondary">
      {{ __('Ara') }}
    </button>
  </form>

  @include('partials.notifications')

  <table class="table table-responsive-xs table-striped">
    <thead>
      <tr>
        <th>{{ __('Sicil No.') }}</th>
        <th>{{ __('Tür') }}</th>
        <th>{{ __('Başlama tarihi') }}</th>
        <th>{{ __('Bitiş tarihi') }}</th>
        <th>{{ __('Bilgi kaynağı') }}</th>
        <th>{{ __('Belge tarihi') }}</th>
        <th>{{ __('Belge Açıklaması') }}</th>
        <th>{{ __('Düşülen ay sayısı') }}</th>
        <th>{{ __('Ay sayısı') }}</th>
        <th>{{ __('Muaf ay sayısı') }}</th>
        <th>{{ __('Eksiltilecek tutar') }}</th>
        <th></th>
      </tr>
    </thead>
    @if($list->count())
      <tfoot class="font-weight-bold">
        <tr>
          <td colspan="7" class="text-right">
            {{ __('Toplam') }}
          </td>
          <td class="text-right">{{ $totalSubstractedMonthCount }}</td>
          <td class="text-right">{{ $totalMonthCount }}</td>
          <td class="text-right">{{ $totalExemptedMonthCount }}</td>
          <td class="text-right">{{ $totalFeeToSubstract }} TL</td>
        </tr>
      </tfoot>
    @endif
    <tbody>
      @foreach($list as $item)
      <tr>
        <td>{{ $item->user->code }}</td>
        <td>{{ $item->exemptionType->title }}</td>
        <td>{{ $item->starts_at->format('d.m.Y') }}</td>
        <td>
          @if(!empty($item->ends_at))
            {{ $item->ends_at->format('d.m.Y') }}
          @else
            <span class="text-muted">{{ __('girilmemiş') }}</span>
          @endif
        </td>
        <td>{{ $item->exemptionSource->title }}</td>
        <td>{{ $item->exemption_source_at->format('d.m.Y') }}</td>
        <td>{{ $item->exemption_info }}</td>
        <td class="text-right">{{ $item->substracted_month_count }}</td>
        <td class="text-right">{{ $item->month_count }}</td>
        <td class="text-right">{{ $item->exempted_month_count }}</td>
        <td class="text-right">{{ $item->fee_to_substract }} TL</td>
        <td class="text-right">
          <a class="btn btn-outline-info" href="{{ route('exemption.edit', $item->id) }}">{{ __('Düzenle') }}</a>
          <form class="d-inline" action="{{ route('exemption.destroy', $item->id) }}" method="post">
            @csrf
            <input type="hidden" name="_method" value="delete">
            <button
              type="submit"
              class="btn btn-outline-danger"
            >{{ __('Sil') }}</button>
          </form>
        </td>
      </tr>
      @endforeach
      @if (!empty($user->id) and !$list->count())
        <tr>
          <td class="text-center text-muted" colspan="12">
            {{ __('Hiç muafiyet kaydı yok.') }}
          </td>
        </tr>
      @elseif (!$list->count())
        <tr>
          <td class="text-center text-muted" colspan="12">
            {{ __('Lütfen sicil no ile arama yapın.') }}
          </td>
        </tr>
      @endif
    </tbody>
  </table>
</div>
@endsection
