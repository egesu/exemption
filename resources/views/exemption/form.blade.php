@extends('layouts.app')

@section('content')
<div class="container">
  <h3 class="mb-5">
    {{ __('Muafiyet Ekle') }}
    - <strong class="text-danger">
      {{ $user->name }} {{ $user->last_name }}
    </strong>
  </h3>

  @include('partials.notifications')

  <form
    method="post"
    @if($exemption->id)
      action="{{ route('exemption.update', $exemption->id) }}"
    @else
      action="{{ route('exemption.store') }}"
    @endif
  >
    @csrf

    @if($exemption->id)
      <input type="hidden" name="_method" value="put">
    @else
      <input type="hidden" name="user_id" value="{{ $user->id }}">
    @endif

    <div class="row">
      <div class="col-12 col-md-6">
        <div class="form-group">
          <label for="user">{{ __('Üye') }}</label>
          <input
            name="user"
            id="user"
            class="form-control form-control-sm"
            disabled
            value="{{ $user->code }} - {{ $user->name }} {{ $user->last_name }}"
          >
        </div>
      </div>
      <div class="col-12 col-md-6">
        <div class="form-group">
          <label for="exemption_type_id">{{ __('Tür') }}</label>
          <select
            name="exemption_type_id"
            id="exemption_type_id"
            class="form-control form-control-sm"
            required
          >
            <option></option>
            @foreach($typeList as $item)
              <option
                value="{{ $item->id }}"
                @if((int)(old('exemption_type_id') ?: $exemption->exemption_type_id) === $item->id)
                  selected
                @endif
              >{{ $item->title }}</option>
            @endforeach
          </select>
        </div>
      </div>
    </div>

    <div class="row">
      <div class="col-12 col-md-6">
        <div class="form-group">
          <label for="starts_at">{{ __('Başlangıç tarihi') }}</label>
          <input
            type="date"
            name="starts_at"
            id="starts_at"
            class="form-control form-control-sm"
            required
            value="{{ old('starts_at') ?: (!empty($exemption->starts_at) ? $exemption->starts_at->format('Y-m-d') : '') }}"
          >
        </div>
      </div>
      <div class="col-12 col-md-6">
        <div class="form-group">
          <label for="ends_at">{{ __('Bitiş tarihi') }}</label>
          <input
            type="date"
            name="ends_at"
            id="ends_at"
            class="form-control form-control-sm"
            value="{{ old('ends_at') ?: (!empty($exemption->ends_at) ? $exemption->ends_at->format('Y-m-d') : '') }}"
          >
        </div>
      </div>
    </div>

    <div class="row">
      <div class="col-12 col-md-6">
        <div class="form-group">
          <label for="exemption_source_id">{{ __('Bilgi kaynağı') }}</label>
          <select
            name="exemption_source_id"
            id="exemption_source_id"
            class="form-control form-control-sm"
            required
          >
            <option></option>
            @foreach($sourceList as $item)
              <option
                value="{{ $item->id }}"
                @if((int)(old('exemption_source_id') ?: $exemption->exemption_source_id) === $item->id)
                  selected
                @endif
              >{{ $item->title }}</option>
            @endforeach
          </select>
        </div>
      </div>
      <div class="col-12 col-md-6">
        <div class="form-group">
          <label for="exemption_source_at">{{ __('Belge tarihi') }}</label>
          <input
            type="date"
            name="exemption_source_at"
            id="exemption_source_at"
            class="form-control form-control-sm"
            required
            value="{{ old('exemption_source_at') ?: (!empty($exemption->exemption_source_at) ? $exemption->exemption_source_at->format('Y-m-d') : '') }}"
          >
        </div>
      </div>
    </div>

    <div class="form-group">
      <label for="name">{{ __('Belge açıklaması') }}</label>
      <textarea
        name="exemption_info"
        id="exemption_info"
        class="form-control form-control-sm"
        required
        maxlength="300"
      >{{ old('exemption_info') ?: $exemption->exemption_info }}</textarea>
    </div>

    <div class="form-group">
      <label for="substracted_month_count">{{ __('Düşülen ay sayısı') }}</label>
      <input
        type="number"
        name="substracted_month_count"
        id="substracted_month_count"
        class="form-control form-control-sm"
        required
        value="{{ old('substracted_month_count') ?: $exemption->substracted_month_count ?: 0 }}"
        min="0"
        max="999"
        step="1"
      >
    </div>

    <div>
      <button type="submit" class="btn btn-primary btn-block">{{ __('Kaydet') }}</button>
    </div>
  </form>
</div>
@endsection
