@extends('layouts.app')

@section('content')
<div class="container">
  <div class="row mb-5">
    <div class="col-12 col-sm-8 text-center text-sm-left">
      <h3>{{ __('Bilgi Kaynakları') }}</h3>
    </div>
    <div class="col-12 col-sm-4 text-center text-sm-right">
      <a class="btn btn-outline-info" href="{{ route('exemption-source.create') }}">
        {{ __('Yeni Bilgi Kaynağı') }}
      </a>
    </div>
  </div>

  @include('partials.notifications')

  <table class="table table-responsive-xs table-striped">
    <thead>
      <tr>
        <th>#</th>
        <th>{{ __('Bilgi Kaynağı') }}</th>
        <th></th>
      </tr>
    </thead>
    <tbody>
      @foreach($list as $item)
      <tr>
        <td>{{ $item->id }}</td>
        <td>{{ $item->title }}</td>
        <td class="text-right">
          <a class="btn btn-outline-info" href="{{ route('exemption-source.edit', $item->id) }}">{{ __('Düzenle') }}</a>
          <form class="d-inline" action="{{ route('exemption-source.destroy', $item->id) }}" method="post">
            @csrf
            <input type="hidden" name="_method" value="delete">
            <button
              type="submit"
              class="btn btn-outline-danger"
            >{{ __('Sil') }}</button>
          </form>
        </td>
      </tr>
      @endforeach
    </tbody>
  </table>
</div>
@endsection
