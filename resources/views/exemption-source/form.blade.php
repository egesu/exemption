@extends('layouts.app')

@section('content')
<div class="container">
  <h3 class="mb-5">{{ __('Bilgi kaynağı') }}</h3>

  @include('partials.notifications')

  <form
    method="post"
    @if($exemptionSource->id)
      action="{{ route('exemption-source.update', $exemptionSource->id) }}"
    @else
      action="{{ route('exemption-source.store') }}"
    @endif
  >
    @csrf

    @if($exemptionSource->id)
      <input type="hidden" name="_method" value="put">
    @endif

    <div class="form-group">
      <label for="title">{{ __('Bilgi kaynağı') }}</label>
      <input
        type="text"
        name="title"
        id="title"
        class="form-control"
        required
        value="{{ old('title') ?: $exemptionSource->title }}"
        maxlength="20"
      >
    </div>

    <div>
      <button type="submit" class="btn btn-primary btn-block">{{ __('Kaydet') }}</button>
    </div>
  </form>
</div>
@endsection
