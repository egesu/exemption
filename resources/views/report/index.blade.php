@extends('layouts.app')

@section('content')
<div class="container">
  <div class="row mb-5">
    <div class="col-12 col-sm-8 text-center text-sm-left">
      <h3>
        {{ __('Rapor') }}
      </h3>
    </div>
    <div class="col-12 col-sm-4 text-center text-sm-right">
      <a
        class="btn btn-outline-info"
        href="{{ route('report.csv', request()->all()) }}"
        target="_blank"
      >{{ __('CSV') }}</a>
    </div>
  </div>

  <form action="{{ route('report.index') }}" method="get" class="mb-3">
    <div class="form-group row">
      <label class="col-sm-2 col-lg-1 col-form-label" for="code_start">
        {{ __('Sicil No.') }}
      </label>
      <div class="col-sm-5 col-md-3 col-lg-2">
        <input
          class="form-control input-sm"
          type="number"
          min="1"
          max="999999"
          step="1"
          name="code_start"
          id="code_start"
          value="{{ request('code_start') }}"
          placeholder="Başlangıç"
        >
      </div>
      <div class="col-sm-5 col-md-3 col-lg-2">
        <input
          class="form-control input-sm"
          type="number"
          min="1"
          max="999999"
          step="1"
          name="code_end"
          id="code_end"
          value="{{ request('code_end') }}"
          placeholder="Bitiş"
        >
      </div>
    </div>
    <div class="form-group row">
      <label class="col-sm-2 col-lg-1 col-form-label" for="exemption_type_id">
        {{ __('Tür') }}
      </label>
      <div class="col-sm-10 col-md-6 col-lg-4">
        <select
          class="form-control input-sm"
          name="exemption_type_id"
          id="exemption_type_id"
        >
          <option value=""></option>
          @foreach($exemptionTypeList as $item)
            <option
              value="{{ $item->id }}"
              @if(((int)request('exemption_type_id')) === $item->id)
                selected
              @endif
            >{{ $item->title }}</option>
          @endforeach
        </select>
      </div>
    </div>
    <div class="form-group row">
      <label class="col-sm-2 col-lg-1 col-form-label" for="name">
        {{ __('Ad - Soyadı') }}
      </label>
      <div class="col-sm-5 col-md-3 col-lg-2">
        <input
          class="form-control input-sm"
          type="text"
          name="name"
          id="name"
          value="{{ request('name') }}"
          placeholder="Ad"
        >
      </div>
      <div class="col-sm-5 col-md-3 col-lg-2">
        <input
          class="form-control input-sm"
          type="text"
          name="last_name"
          id="last_name"
          value="{{ request('last_name') }}"
          placeholder="Soyadı"
        >
      </div>
    </div>

    <button type="submit" class="btn btn-outline-secondary">
      {{ __('Ara') }}
    </button>
  </form>

  @include('partials.notifications')

  <table class="table table-responsive-xs table-striped">
    <thead>
      <tr>
        <th>{{ __('Sicil no.') }}</th>
        <th>{{ __('Ad') }}</th>
        <th>{{ __('Soyadı') }}</th>
        <th>{{ __('Güncel muafiyet') }}</th>
        <th>{{ __('Başlama tarihi') }}</th>
        <th>{{ __('Toplam ay sayısı') }}</th>
        <th>{{ __('Toplam düşülen ay sayısı') }}</th>
        <th>{{ __('Toplam muaf ay sayısı') }}</th>
        <th>{{ __('Toplam eksiltilecek tutar') }}</th>
      </tr>
    </thead>
    <tbody>
      @foreach($list as $item)
      <tr>
        <td>{{ $item->code }}</td>
        <td>{{ $item->name }}</td>
        <td>{{ $item->last_name }}</td>
        <td>
          @if(!empty($item->currentExemption))
            {{ $item->currentExemption->exemptionType->title }}
          @else
            <em class="text-muted">{{ __('Yok') }}</em>
          @endif
        </td>
        <td>
          @if(!empty($item->currentExemption))
            {{ $item->currentExemption->starts_at->format('d.m.Y') }}
          @else
            <em class="text-muted">{{ __('Yok') }}</em>
          @endif
        </td>
        <td>
          {{ $item->totalMonthCount }}
        </td>
        <td>
          {{ $item->totalSubstractedMonthCount }}
        </td>
        <td>
          {{ $item->totalExemptedMonthCount }}
        </td>
        <td>
          {{ $item->totalFeeToSubstract }} TL
        </td>
      </tr>
      @endforeach
    </tbody>
  </table>
</div>
@endsection
