<?php

use Illuminate\Database\Seeder;

class FakeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // factory(\App\Models\User::class, 3)
        //     ->create([
        //         'password' => bcrypt('123456'),
        //     ]);

        // factory(\App\Models\Fee::class, 1)
        //     ->create([
        //         'starts_at' => '2000-01-01',
        //         'ends_at' => '2010-01-31',
        //     ]);
        //
        // factory(\App\Models\Fee::class, 1)
        //     ->create([
        //         'starts_at' => '2010-02-01',
        //         'ends_at' => '2015-03-15',
        //     ]);
        //
        // factory(\App\Models\Fee::class, 1)
        //     ->create([
        //         'starts_at' => '2015-03-15',
        //         'ends_at' => null,
        //     ]);
        //
        // factory(\App\Models\ExemptionType::class, 5)
        //     ->create();

        factory(\App\Models\ExemptionSource::class, 5)
            ->create();
    }
}
