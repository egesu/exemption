<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExemptionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('exemptions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('exemption_type_id')->unsigned();
            $table->bigInteger('exemption_source_id')->unsigned();
            $table->bigInteger('user_id')->unsigned();
            $table->date('starts_at');
            $table->date('ends_at')->nullable();
            $table->date('exemption_source_at');
            $table->string('exemption_info', 500)->nullable();
            $table->decimal('substracted_month_count', 3, 0)->nullable();
            $table->timestamps();

            $table->foreign('exemption_type_id')
                ->references('id')->on('exemption_types')
                ->onUpdate('restrict')->onDelete('restrict');

            $table->foreign('exemption_source_id')
                ->references('id')->on('exemption_sources')
                ->onUpdate('restrict')->onDelete('restrict');

            $table->foreign('user_id')
                ->references('id')->on('users')
                ->onUpdate('restrict')->onDelete('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('exemptions');
    }
}
