<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Fee;
use Faker\Generator as Faker;

$factory->define(Fee::class, function (Faker $faker) {
    return [
        'starts_at' => $faker->date(),
        'ends_at' => $faker->date(),
        'amount' => $faker->randomFloat(2, 5, 25),
    ];
});
