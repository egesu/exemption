<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\ExemptionSource;
use Faker\Generator as Faker;

$factory->define(ExemptionSource::class, function (Faker $faker) {
    return [
        'title' => $faker->text(20),
    ];
});
