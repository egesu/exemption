<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\ExemptionType;
use Faker\Generator as Faker;

$factory->define(ExemptionType::class, function (Faker $faker) {
    return [
        'title' => $faker->text(20),
    ];
});
