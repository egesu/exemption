<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes([
    'register' => false,
    'reset' => false,
    'verify' => false,
]);

Route::get('/', 'HomeController@index')
    ->name('home')
    ->middleware('auth');
Route::resource('fee', 'FeeController')
    ->middleware('auth');
Route::resource('exemption-type', 'ExemptionTypeController')
    ->middleware('auth');
Route::resource('exemption-source', 'ExemptionSourceController')
    ->middleware('auth');
Route::resource('user', 'UserController')
    ->middleware('auth');
Route::resource('exemption', 'ExemptionController')
    ->middleware('auth');
Route::get('report', 'ReportController@index')
    ->name('report.index')
    ->middleware('auth');
Route::get('report/csv', 'ReportController@getCSV')
    ->name('report.csv')
    ->middleware('auth');

Route::get('/debug-sentry', function () {
    throw new Exception('My first Sentry error!');
});
